
import socket

'''
    Sockets

    Contains methods used to connect to Unix Domain or TCP sockets.
'''

timeout = 5

def setup_domain_socket(location):

    '''
        Setup Domain Socket

        Setup a connection to a Unix Domain Socket

        --
        @param  location:str    The path to the Unix Domain Socket to connect to.
        @return <class 'socket._socketobject'>
    '''

    clientsocket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    clientsocket.settimeout(timeout)
    clientsocket.connect(location)

    return clientsocket

def setup_tcp_socket(location, port):

    '''
        Setup TCP Socket

        Setup a connection to a TCP Socket

        --
        @param  location:str    The Hostname / IP Address of the remote TCP Socket.
        @param  port:int        The TCP Port the remote Socket is listening on.
        @return <class 'socket._socketobject'>
    '''

    clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientsocket.settimeout(timeout)
    clientsocket.connect((location, port))

    return clientsocket
