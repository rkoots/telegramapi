
'''
    Strings

    Contains methods used to manipulate strings
'''

def escape_recipient(recipient):

    '''
        Escape Recipient

        Replaces spaces( ) with underscores(_) to match the telegram-cli format

        --
        @param  recipient:str   The recipient to fix up.
        @return str
    '''
    return recipient.replace(' ','_')

def escape_newlines(string):

    '''
        Escape Newlines

        Replaces newlines with a literal '\\n' so that they may be sent over
        the socket. The socket does not support newlines.

        --
        @param  string:str  The strings to escape.
        @return str
    '''
    return string.replace('\n', '\\n')
